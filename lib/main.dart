import 'package:flutter/material.dart';
import 'package:flutter_app/models/cart_model.dart';
import 'package:flutter_app/models/user_model.dart';
import 'package:flutter_app/screens/home_screen.dart';
import 'package:scoped_model/scoped_model.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScopedModel<UserModel>(
      model: UserModel(),
      child: ScopedModelDescendant<UserModel>(builder: (context, child, model) {
        return ScopedModel<CartModel>(
          model: CartModel(model),
          child: MaterialApp(
              title: "Loja LS",
              theme: ThemeData(
                  primarySwatch: Colors.blue, primaryColor: Colors.lightBlue),
              debugShowCheckedModeBanner: false,
              home: HomeScreen()),
        );
      }),
    );
  }
}
